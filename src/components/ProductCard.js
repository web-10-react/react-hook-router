import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Carousel from "react-bootstrap/Carousel";
function ProductCard(props) {
  return (
    <Card style={{ width: "18rem" }}>
      <Carousel variant="dark">
       
       {
        props.product.images.map((pic)=>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={pic}
            alt="First slide"
          />
        </Carousel.Item>
        )
       }
       
       
      </Carousel>

      <Card.Body>
        <Card.Title>{props.product.title}</Card.Title>
        <Card.Text>
           {props.product.description}
        </Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
  );
}

export default ProductCard;
