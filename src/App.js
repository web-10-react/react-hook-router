import logo from "./logo.svg";
import "./App.css";
import MyNavBar from "./components/MyNavBar";
import MyCard from "./components/MyCard";
import "bootstrap/dist/css/bootstrap.min.css";
import AllUsers from "./pages/AllUsers";
import ProductCard from "./components/ProductCard";
import AllProducts from "./pages/AllProducts";
import { BrowserRouter as BigRouter, Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";

function App() {
  return (
    <div>
      <BigRouter>
        <MyNavBar />
        <Routes>
          <Route path="/" index element={<HomePage />} />
          <Route path="/users" element={<AllUsers />} />
          <Route path="/products" element={<AllProducts />} />
        </Routes>
      </BigRouter>
    </div>
  );
}

export default App;
