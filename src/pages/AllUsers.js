import axios from "axios";
import React from "react";
import { useState, useEffect } from "react";
import MyCard from "../components/MyCard";
import { MutatingDots } from "react-loader-spinner";
const AllUsers = () => {
  const [users, setUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    // fetch users and add it into users state
    axios
      .get("https://api.escuelajs.co/api/v1/users")
      .then((response) => {
        setUsers(response.data);
        setIsLoading(false);
      })
      .catch((error) => console.log("Error : ", error));
  }, []); // only once

  console.log("ALL users are : ", users);
  return (
    <div className="container">
      <h1>All Users </h1>
      {isLoading ? (
        // loading
        // else
        // all users
        <div className="d-flex justify-content-center">
          <MutatingDots
            height="100"
            width="100"
            color="orange"
            secondaryColor="#4fa94d"
            radius="12.5"
            ariaLabel="mutating-dots-loading"
            wrapperStyle={{}}
            wrapperClass=""
            visible={true}
          />
        </div>
      ) : (
        <div className="row">
          {users.map((user) => (
            <div className="col-4">
              <MyCard user={user} />
            </div>
          ))}
        </div>
      )}
    </div>
  );
};
export default AllUsers;
