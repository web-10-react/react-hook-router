import axios from "axios";
import React, { useState, useEffect } from "react";
import ProductCard from "../components/ProductCard";
import { Bars } from "react-loader-spinner";
const AllProducts = () => {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
//   const [searchValue, setSearchValue] = useState("");
  useEffect(() => {
    axios
      .get("https://api.escuelajs.co/api/v1/products/")
      .then((response) => {
        setProducts(response.data);
        setIsLoading(false);
      })
      .catch((error) => console.log("Error is : ", error));
  }, []);
  const handleSearch = (e) => {
    setProducts(
      products.filter((product) => product.title.startsWith(e.target.value))
    );
  };
  return (
    <div className="container">
      {isLoading ? (
        <div className="loading d-flex justify-content-center">
          <Bars
            height="80"
            width="80"
            color="orange"
            ariaLabel="bars-loading"
            wrapperStyle={{}}
            wrapperClass=""
            visible={true}
          />
        </div>
      ) : (
        <div className="row">
          <div className="container p-4 d-flex justify-content-end">
            <input
              onChange={(e) => handleSearch(e)}
              className="form form-control w-25"
              type="text"
              placeholder="Search category"
            />
          </div>

          {products.map((product) => (
            <div className="col-4 d-flex justify-content-center">
              <ProductCard product={product} />
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default AllProducts;
